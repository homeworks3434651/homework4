package org.homework4;

import java.util.Arrays;

public class Main {

    public static void main(String[] args){

        // Family 1
        Family family1 = new Family(new Human("John", "Doe", 1970),
                                    new Human("Jane", "Doe", 1975));

        Pet pet_family1 = new Pet();
        pet_family1.setSpecies("Labrador");
        pet_family1.setNickname("Buddy");
        pet_family1.setAge(4);
        pet_family1.setTrickLevel(55);
        String[] pet_family1_habits = {"eat", "drink", "sleep"};
        pet_family1.setHabits(pet_family1_habits);

        family1.setPet(pet_family1);

        // Use getters
        System.out.println("\nPet in Family 1:");
        System.out.println(family1.getPet());
        System.out.println("\nHabits of pet_for_child_family1:");
        System.out.println(Arrays.toString(pet_family1.getHabits(pet_family1_habits)));

        pet_family1.eat();
        pet_family1.respond();
        pet_family1.foul();

        Human child_family1 = new Human("Alex", "Doe", 2000, 110, family1, new String[][]{{"Monday", "School"}, {"Friday", "Play"}});
        System.out.println("\nChild in Family 1:");
        System.out.println(child_family1);
        child_family1.greetPet();
        child_family1.describePet();
        System.out.println();

        family1.getFather().feedPet(false);

        family1.addChild(child_family1);
        family1.addChild(new Human("Bob", "Doe", 1995));
        family1.addChild(new Human("Davida", "Doe", 2000, family1));

        System.out.println("\nFamily 1 (with pets and children):");
        System.out.println(family1);
        System.out.println();

        family1.deleteChild(family1.getChildren()[0]);
        family1.deleteChild(new Human("Bob", "Doe", 1995));   // I get the error message
        family1.deleteChild(new Human("Bob", "Doe", 1995));   // I get the error message
        family1.deleteChild(1);
        family1.deleteChild(family1.getChildren()[0]);
        try {
            family1.deleteChild(family1.getChildren()[0]);
        } catch (IndexOutOfBoundsException e) {
            System.out.println(e.getMessage());    // I get the exception message
        }
        family1.deleteChild(0);  // I get the error message
        try {
            family1.deleteChild(family1.getChild(10));
        } catch (IndexOutOfBoundsException e) {
            System.out.println(e.getMessage());    // I get the exception message
        }

        Human[] newChildren_family1 = {
                new Human("Emily", "Doe", 2012),
                new Human("Brandon", "Doe", 2014, family1),
                new Human("Landon", "Doe", 2017)
        };

        family1.setChildren(newChildren_family1);

        // Create a second Pet for family1
        Pet pet_family1_second = new Pet("Golden Retriever", "Max");
        pet_family1_second.setAge(3);

        pet_family1_second.setTrickLevel(40);
        pet_family1_second.setHabits(new String[]{"play", "fetch"});
        family1.setPet(pet_family1_second);

        // Use getters
        System.out.println("\nAnother Pet in Family 1:");
        System.out.println(family1.getPet());
        System.out.println();

        family1.getChildren()[1].feedPet(true);

        Human lastChild = family1.getChildren()[family1.getChildren().length - 1];
        System.out.println("\nLast Child in Family 1:");
        System.out.println(lastChild);
        lastChild.greetPet();
        lastChild.describePet();

        System.out.println("\nFamily 1 after setting new children and the new pet:");
        System.out.println(family1);

        // Set a new father
        Human newFather_family1 = new Human("Bob", "Kennedy", 1970);
        family1.setFather(newFather_family1);  // I get the error message

        // Family 2
        Human father_family2 = new Human("Bob", "Smith", 1975);
        Human mother_family2 = new Human("Alice", "Smith", 1980);
        Family family2 = new Family(father_family2, mother_family2);

        Pet pet_family2 = new Pet("Persian Cat", "Whiskers");
        family2.setPet(pet_family2);

        Human child_family2 = new Human("Ryan", "Smith", 2005, family2);
        family2.addChild(child_family2);
        System.out.println("\nChild in Family 2:");
        System.out.println(child_family2);

        child_family2.setFamily(family1);  // I get the error message

        family2.addChild(new Human("Debora", "Smith", 2010));
        family2.addChild(new Human("Ruth", "Smith", 2015));

        pet_family2.setTrickLevel(60);
        System.out.println("\nPet in Family 2:");
        System.out.println(pet_family2);
        System.out.println();

        family2.getChildren()[2].feedPet(false);

        System.out.println("\nFamily 2 (with pets and children):");
        System.out.println(family2);

        // Set a new mother
        Human newMother = new Human("Marina", "Krotova", 1980, family2);
        family2.setMother(newMother);   // I get the error message
        System.out.println();

        // Family 3
        Family family3 = new Family(new Human("Charlie", "Brown", 1990),
                                    new Human("Lucy", "Brown", 1992));

        Human son_family3 = new Human("Bryan", "Brown", 2010, 120, family3, new String[][]{{"Monday", "Studying"}, {"Friday", "Gym"}});
        family3.addChild(son_family3);
        System.out.println("Son in Family 3:");
        System.out.println(son_family3);

        // Use getters
        Human sonFamily3 = family3.getChildren()[0];

        System.out.println("\nSon in Family 3 - IQ: " + sonFamily3.getIQ());
        System.out.println("Son in Family 3 - Schedule: " + Arrays.deepToString(sonFamily3.getSchedule()));

        Pet pet_family3 = new Pet(3, "Parrot", "Polly", 70, new String[]{"talk", "fly"});
        family3.setPet(pet_family3);
        System.out.println("Son in Family 3 - Pet: " + family3.getPet());
        pet_family3.eat();
        pet_family3.respond();
        pet_family3.foul();
        family3.getMother().feedPet(false);

        Human daughter_family3 = new Human();

        daughter_family3.setName("Laura");
        daughter_family3.setSurname("Brown");
        daughter_family3.setYear(2006);
        daughter_family3.setIQ(100);
        daughter_family3.setFamily(family3);
        String[][] scheduleForLaura = {{"Monday", "School"}, {"Friday", "Dance Class"}};
        daughter_family3.setSchedule(scheduleForLaura);

        family3.addChild(daughter_family3);
        System.out.println("\nDaughter in Family 3:");
        System.out.println(daughter_family3);

        System.out.println("\nFamily 3 (with pets and children):");
        System.out.println(family3);
        System.out.println();

        // False families
        Family false_family1 = new Family(father_family2, new Human("Marina", "Krotova", 1980));
        System.out.println(false_family1);  // I get the error message

        Family false_family2 = new Family(new Human("Charlie", "Brown", 1990), mother_family2);
        System.out.println(false_family2);  // I get the error message

        Family false_family3 = new Family(new Human("Mike", "Jackson", 1985),
                                          new Human("Mike", "Jackson", 1985));
        System.out.println(false_family3);  // I get the error message

        // Invalid family and fixing it
        Family valid_family = new Family(null, null);
        System.out.println(valid_family.getFather());
        System.out.println(valid_family.getMother());

        // Set a mother
        Human mother_valid_family = new Human("Irin", "Peterson", 1981);
        valid_family.setMother(mother_valid_family);
        System.out.println(valid_family.getMother());

        // Set a father
        Human father_invalid_family = new Human("Ted", "Jansen", 1976);
        valid_family.setFather(father_invalid_family);
        System.out.println(valid_family.getFather());
        System.out.println(valid_family);
    }
}
