package org.homework4;

import java.util.Arrays;
import java.util.Objects;

public class Family {
    private Human father;
    private Human mother;
    private Human[] children;
    private Pet pet;

    private boolean parentsSet = false;

    private String errorMessage;

    static{
        System.out.println("Family class is loaded.");
    }

    {
        System.out.println("A new Family object is created.");
    }

    public Family(Human father, Human mother){
        if (father != null && mother != null) {
            if (father.equals(mother)) {
                errorMessage = "Error: Father and mother cannot be the same person.";
                printErrorMessage();
                return;
            }

            if (father.getFamily() != null || mother.getFamily() != null) {
                errorMessage = "Error: One or both parents are already part of another family.";
                printErrorMessage();
                return;
            }

            this.father = father;
            this.mother = mother;
            this.father.setFamily(this);
            this.mother.setFamily(this);
            this.children = new Human[0];
            parentsSet = true;
        } else if (father == null && mother == null) {
            errorMessage = "Invalid family creation. Please ensure both parents are provided and not null.";
            printErrorMessage();
            this.father = null;
            this.mother = null;
            this.children = null;
            parentsSet = false;
        } else if (father == null) {
            errorMessage = "Error: Father cannot be null.";
            printErrorMessage();
            this.father = null;
            this.mother = mother;
            this.children = null;
            parentsSet = false;
        } else {
            errorMessage = "Error: Mother cannot be null.";
            printErrorMessage();
            this.father = father;
            this.mother = null;
            this.children = null;
            parentsSet = false;
        }
    }

    private void printErrorMessage(){
        if (errorMessage != null) {
            System.out.println(errorMessage);
        }
    }

    public boolean addChild(Human child){
        if (Arrays.asList(children).contains(child)) {
            System.out.println("Error: This child is already part of the family.");
            return false;
        }
        child.setFamily(this);

        Human[] newChildren = Arrays.copyOf(children, children.length + 1);
        newChildren[children.length] = child;
        children = newChildren;
        return true;
    }

    public void deleteChild(Human child){
        if (child == null) {
            System.out.println("Child is null. Cannot delete null child.");
            return;
        }

        if (children != null) {
            int index = -1;
            for (int i = 0; i < children.length; i++) {
                if (children[i].equals(child)) {
                    index = i;
                    break;
                }
            }
            if (index != -1) {
                Human[] newChildren = new Human[children.length - 1];
                System.arraycopy(children, 0, newChildren, 0, index);
                System.arraycopy(children, index + 1, newChildren, index, children.length - index - 1);
                children = newChildren;

                System.out.println("Child deleted successfully.");
                System.out.println("Family after deleting child:");
                System.out.println(this);
            } else {
                System.out.println("Child not found in the family.");
            }
        }
    }

    public Human getChild(int index){
        if (index >= children.length || index < 0) {
            throw new IndexOutOfBoundsException("There's no index to find.\n");
        }
        return children[index];
    }

    public boolean deleteChild(int index){
        if (children != null && index >= 0 && index < children.length) {
            Human[] newChildren = new Human[children.length - 1];
            System.arraycopy(children, 0, newChildren, 0, index);
            System.arraycopy(children, index + 1, newChildren, index, children.length - index - 1);
            children = newChildren;

            System.out.println("Child at index " + index + " deleted successfully.");
            System.out.println("Family after deleting child at index " + index + ":");
            System.out.println(this);

            return true;
        } else {
            System.out.println("Could not delete child at index " + index + ".");
            return false;
        }
    }

    public int countFamily(){
        int parentsCount = 0;

        if (father != null) {
            parentsCount++;
        }

        if (mother != null) {
            parentsCount++;
        }

        return parentsCount + (children != null ? children.length : 0);
    }

    public Pet getPet(){
        return pet;
    }

    public void setPet(Pet pet){
        this.pet = pet;
    }

    public Human getFather(){
        return father;
    }

    public void setFather(Human father){
        if (!parentsSet) {
            this.father = father;
            father.setFamily(this);
            parentsSet = true;
        } else {
            if (this.father == null) {
                this.father = father;
                father.setFamily(this);
            } else {
                System.out.println("Error: Family already has two parents.");
            }
        }
    }

    public Human getMother(){
        return mother;
    }

    public void setMother(Human mother){
        if (!parentsSet) {
            this.mother = mother;
            mother.setFamily(this);
            parentsSet = true;
        } else {
            if (this.mother == null) {
                this.mother = mother;
                mother.setFamily(this);
            } else {
                System.out.println("Error: Family already has two parents.");
            }
        }
    }

    public Human[] getChildren(){
        return children;
    }

    public void setChildren(Human[] newChildren){
        Human[] combinedChildren = Arrays.copyOf(children, children.length + newChildren.length);
        System.arraycopy(newChildren, 0, combinedChildren, children.length, newChildren.length);

        for (Human child : combinedChildren) {
            child.setFamily(this);
        }

        children = combinedChildren;
    }

    @Override
    public boolean equals(Object o){
        if (this == o) return true;
        if (!(o instanceof Family)) return false;

        Family family = (Family) o;

        boolean parentsEqual = Objects.equals(getFather(), family.getFather()) &&
                Objects.equals(getMother(), family.getMother());

        boolean childrenEqual = Arrays.equals(getChildren(), family.getChildren());

        return parentsEqual && childrenEqual;
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(getFather(), getMother());
        result = 31 * result + Arrays.hashCode(getChildren());
        return result;
    }

    @Override
    public String toString(){
        StringBuilder result = new StringBuilder();

        if (father != null && mother != null) {
            int totalPersons = countFamily();

            if (totalPersons > 0) {
                result.append("Family{\n");
                result.append("  Father: ").append(father).append("\n");
                result.append("  Mother: ").append(mother).append("\n");

                if (children != null && children.length > 0) {
                    // Sort children by age (bubble sort)
                    for (int i = 0; i < children.length - 1; i++) {
                        for (int j = 0; j < children.length - i - 1; j++) {
                            if (children[j].getYear() > children[j + 1].getYear()) {
                                Human temp = children[j];
                                children[j] = children[j + 1];
                                children[j + 1] = temp;
                            }
                        }
                    }
                    result.append("  Children: ").append(Arrays.toString(children)).append("\n");
                } else {
                    result.append("  Children: No children in this family.\n");
                }

                result.append("  Pet: ").append(pet).append("\n");
                result.append("  Total Persons in Family: ").append(totalPersons).append("\n");
                result.append("}");
            }
        }

        return result.toString();
    }
}
