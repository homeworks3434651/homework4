package org.homework4;

import java.util.Arrays;
import java.util.Objects;
import java.util.Random;

public class Human {
    private String name;
    private String surname;
    private int year;
    private int iq;
    private String[][] schedule;
    private Family family;

    static {
        System.out.println("Human class is loaded.");
    }

    {
        System.out.println("A new Human object is created.");
    }

    public Human(){

    }

    public Human(String name, String surname, int year){
        this();
        this.name = name;
        this.surname = surname;
        this.year = year;
    }

    public Human(String name, String surname, int year, Family family){
        this(name, surname, year);
        this.family = family;
    }

    public Human(String name, String surname, int year, int iq, Family family, String[][] schedule){
        this(name, surname, year, family);
        this.iq = iq;
        this.schedule = schedule;
    }

    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name = name;
    }

    public String getSurname(){
        return surname;
    }

    public void setSurname(String surname){
        this.surname = surname;
    }

    public int getYear(){
        return year;
    }

    public void setYear(int year){
        this.year = year;
    }

    public int getIQ(){
        return iq;
    }

    public void setIQ(int iq){
        this.iq = iq;
    }

    public String[][] getSchedule(){
        return schedule;
    }

    public void setSchedule(String[][] schedule){
        this.schedule = schedule;
    }

    public Family getFamily(){
        return family;
    }

    public boolean setFamily(Family family){
        if (this.family != null && !this.family.equals(family)) {
            System.out.println("Error: This human is already part of another family.");
            return false;
        }

        this.family = family;

        return true;
    }

    public void greetPet(){
        if (this.family != null && this.family.getPet() != null) {
            System.out.println("Hello, " + this.family.getPet().getNickname());
        } else {
            System.out.println("I don't have a pet.");
        }
    }

    public void describePet(){
        if (this.family != null && this.family.getPet() != null) {
            String species = this.family.getPet().getSpecies();
            int age = this.family.getPet().getAge();
            int cunningLevel = this.family.getPet().getTrickLevel();

            String cunningDescription = (cunningLevel > 50) ? "very cunning" : "almost not cunning";

            System.out.println("I have a " + species + ". It is " + age + " years old, and it is " + cunningDescription + ".");
        } else {
            System.out.println("I don't have a pet.");
        }
    }

    public boolean feedPet(boolean isTimeToFeed){
        if (this.family != null && this.family.getPet() != null) {
            if (isTimeToFeed) {
                System.out.println("Hmm... I will feed " + this.family.getPet().getNickname());
                return true;
            } else {
                Random random = new Random();
                int randomTrick = random.nextInt(101);
                int petTrickLevel = this.family.getPet().getTrickLevel();

                if (petTrickLevel > randomTrick) {
                    System.out.println("Hmm... I will feed " + this.family.getPet().getNickname());
                    return true;
                } else {
                    System.out.println("I think " + this.family.getPet().getNickname() + " is not hungry.");
                    return false;
                }
            }
        } else {
            System.out.println("I don't have a pet.");
            return false;
        }
    }

    @Override
    public boolean equals(Object o){
        if (this == o) return true;
        if (!(o instanceof Human)) return false;

        Human human = (Human) o;

        return getYear() == human.getYear() &&
                Objects.equals(getName(), human.getName()) &&
                Objects.equals(getSurname(), human.getSurname()) &&
                Objects.equals(getFamily(), human.getFamily());
    }

    @Override
    public int hashCode(){
        return Objects.hash(getName(), getSurname(), getYear(), getFamily());
    }

    @Override
    public String toString(){
        return "Human{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", year=" + year +
                ", iq=" + iq +
                ", schedule=" + Arrays.deepToString(schedule) +
                '}';
    }
}
